package org.gtk.gtk.filter.multi

import gtk.GtkEveryFilter_autoptr
import gtk.gtk_every_filter_new
import kotlinx.cinterop.reinterpret

/**
 * gtk-kt
 *
 * 21 / 11 / 2021
 *
 * @see <a href="https://docs.gtk.org/gtk4/class.EveryFilter.html">
 *     GtkEveryFilter</a>
 */
class EveryFilter(val anyFilterPointer: GtkEveryFilter_autoptr) :
	MultiFilter(anyFilterPointer.reinterpret()) {

	constructor() : this(gtk_every_filter_new()!!)
}