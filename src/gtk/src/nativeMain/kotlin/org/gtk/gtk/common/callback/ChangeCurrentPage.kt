package org.gtk.gtk.common.callback

import glib.gpointer
import gobject.GCallback
import kotlinx.cinterop.asStableRef
import kotlinx.cinterop.reinterpret
import kotlinx.cinterop.staticCFunction
import org.gtk.glib.gtk

/*
 * gtk-kt
 *
 * 18 / 11 / 2021
 */
typealias TypedChangeCurrentPageFunction<T> = T.(Int) -> Boolean