package org.gtk.gtk.filter.multi

import gtk.GtkAnyFilter_autoptr
import gtk.gtk_any_filter_new
import kotlinx.cinterop.reinterpret

/**
 * gtk-kt
 *
 * 21 / 11 / 2021
 *
 * @see <a href="https://docs.gtk.org/gtk4/class.AnyFilter.html">
 *     GtkAnyFilter</a>
 */
class AnyFilter(val anyFilterPointer: GtkAnyFilter_autoptr) :
	MultiFilter(anyFilterPointer.reinterpret()) {

	constructor() : this(gtk_any_filter_new()!!)
}