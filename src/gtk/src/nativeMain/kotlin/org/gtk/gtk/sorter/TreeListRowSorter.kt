package org.gtk.gtk.sorter

import gtk.GtkTreeListRowSorter_autoptr
import gtk.gtk_tree_list_row_sorter_get_sorter
import gtk.gtk_tree_list_row_sorter_new
import gtk.gtk_tree_list_row_sorter_set_sorter
import kotlinx.cinterop.reinterpret

/**
 * 17 / 12 / 2021
 *
 * @see <a href=""></a>
 */
class TreeListRowSorter(
	val treeListPointer: GtkTreeListRowSorter_autoptr,
) : Sorter(treeListPointer.reinterpret()) {

	constructor(sorter: Sorter? = null) : this(gtk_tree_list_row_sorter_new(sorter?.sorterPointer)!!)

	var sorter: Sorter?
		get() = gtk_tree_list_row_sorter_get_sorter(treeListPointer).wrap()
		set(value) = gtk_tree_list_row_sorter_set_sorter(treeListPointer, value?.sorterPointer)
}