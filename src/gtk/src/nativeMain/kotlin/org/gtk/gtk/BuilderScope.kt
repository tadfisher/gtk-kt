package org.gtk.gtk

import gtk.GtkBuilderScope_autoptr

/**
 * 15 / 01 / 2022
 *
 * @see <a href="https://docs.gtk.org/gtk4/iface.BuilderScope.html">
 *     GtkBuilderScope</a>
 */
interface BuilderScope {
	val builderScopePointer: GtkBuilderScope_autoptr

	companion object {
		private class Impl(
			override val builderScopePointer: GtkBuilderScope_autoptr,
		) : BuilderScope

		inline fun GtkBuilderScope_autoptr?.wrap() =
			this?.wrap()

		fun GtkBuilderScope_autoptr.wrap(): BuilderScope =
			Impl(this)
	}
}