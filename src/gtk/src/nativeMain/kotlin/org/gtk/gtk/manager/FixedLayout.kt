package org.gtk.gtk.manager

import gtk.GtkFixedLayout_autoptr
import gtk.gtk_fixed_layout_new
import kotlinx.cinterop.reinterpret

/**
 * kotlinx-gtk
 *
 * 22 / 11 / 2021
 *
 * @see <a href="https://docs.gtk.org/gtk4/class.FixedLayout.html">
 *     GtkFixedLayout</a>
 */
class FixedLayout(
	val fixedLayoutPointer: GtkFixedLayout_autoptr
) : LayoutManager(fixedLayoutPointer.reinterpret()) {

	constructor() : this(gtk_fixed_layout_new()!!.reinterpret())
}