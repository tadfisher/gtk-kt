package org.gtk.gtk.common.enums

import gtk.GtkJustification
import gtk.GtkJustification.*

/**
 * 06 / 03 / 2021
 *
 * @see <a href="https://docs.gtk.org/gtk4/enum.Justification.html">
 *     GtkJustification</a>
 */
enum class Justification(val gtk: GtkJustification) {
	LEFT(GTK_JUSTIFY_LEFT),
	RIGHT(GTK_JUSTIFY_RIGHT),
	CENTER(GTK_JUSTIFY_CENTER),
	FILL(GTK_JUSTIFY_FILL);

	companion object {
		fun valueOf(gtk: GtkJustification) =
			when (gtk) {
				GTK_JUSTIFY_LEFT -> LEFT
				GTK_JUSTIFY_RIGHT -> RIGHT
				GTK_JUSTIFY_CENTER -> CENTER
				GTK_JUSTIFY_FILL -> FILL
			}
	}
}