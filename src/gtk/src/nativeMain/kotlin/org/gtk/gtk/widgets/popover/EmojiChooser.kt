package org.gtk.gtk.widgets.popover

import glib.gpointer
import gobject.GCallback
import gtk.GTK_TYPE_EMOJI_CHOOSER
import gtk.GtkEmojiChooser
import gtk.gtk_emoji_chooser_new
import kotlinx.cinterop.*
import org.gtk.glib.CStringPointer
import org.gtk.gobject.Signals.EMOJI_PICKED
import org.gtk.gobject.addSignalCallback
import org.gtk.gobject.typeCheckInstanceCastOrThrow
import org.gtk.gtk.widgets.Widget

/**
 * 15 / 12 / 2021
 *
 * @see <a href="https://docs.gtk.org/gtk4/class.EmojiChooser.html">
 *     GtkEmojiChooser</a>
 */
class EmojiChooser(
	val emojiChooserPointer: CPointer<GtkEmojiChooser>,
) : Popover(emojiChooserPointer.reinterpret()) {

	constructor(widget: Widget) :
			this(typeCheckInstanceCastOrThrow(widget, GTK_TYPE_EMOJI_CHOOSER))

	constructor() : this(gtk_emoji_chooser_new()!!.reinterpret())

	fun addOnEmojiPickedCallback(action: EmojiChooserEmojiPickedFunction) =
		addSignalCallback(EMOJI_PICKED, action, staticEmojiChooserFunction)

	companion object {
		inline fun CPointer<GtkEmojiChooser>?.wrap() =
			this?.wrap()

		inline fun CPointer<GtkEmojiChooser>.wrap() =
			EmojiChooser(this)

		private val staticEmojiChooserFunction: GCallback =
			staticCFunction {
					self: CPointer<GtkEmojiChooser>,
					text: CStringPointer,
					data: gpointer,
				->
				data.asStableRef<EmojiChooserEmojiPickedFunction>()
					.get()
					.invoke(self.wrap(), text.toKString())
			}.reinterpret()
	}
}

typealias EmojiChooserEmojiPickedFunction = EmojiChooser.(emoji: String) -> Unit