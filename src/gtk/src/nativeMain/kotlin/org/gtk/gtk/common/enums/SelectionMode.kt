package org.gtk.gtk.common.enums

import gtk.GtkSelectionMode
import gtk.GtkSelectionMode.*

/**
 * 06 / 03 / 2021
 *
 * @see <a href="https://docs.gtk.org/gtk4/enum.SelectionMode.html">
 *     GtkSelectionMode</a>
 */
enum class SelectionMode(val gtk: GtkSelectionMode) {
	NONE(GTK_SELECTION_NONE),
	SINGLE(GTK_SELECTION_SINGLE),
	BROWSE(GTK_SELECTION_BROWSE),
	MULTIPLE(GTK_SELECTION_MULTIPLE);

	companion object {

		fun valueOf(gtk: GtkSelectionMode) =
			when (gtk) {
				GTK_SELECTION_NONE -> NONE
				GTK_SELECTION_SINGLE ->SINGLE
				GTK_SELECTION_BROWSE ->BROWSE
				GTK_SELECTION_MULTIPLE ->MULTIPLE
			}
	}
}