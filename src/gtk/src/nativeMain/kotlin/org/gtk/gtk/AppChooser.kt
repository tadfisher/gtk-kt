package org.gtk.gtk

import gtk.GtkAppChooser_autoptr
import gtk.gtk_app_chooser_get_app_info
import gtk.gtk_app_chooser_get_content_type
import gtk.gtk_app_chooser_refresh
import kotlinx.cinterop.pointed
import kotlinx.cinterop.value
import org.gtk.gio.AppInfo
import org.gtk.gio.AppInfo.Companion.wrap
import org.gtk.glib.use

/**
 * gtk-kt
 *
 * 19 / 10 / 2021
 *
 * @see <a href="https://docs.gtk.org/gtk4/iface.AppChooser.html">
 *     GtkAppChooser</a>
 */
interface AppChooser {

	val appChooserPointer: GtkAppChooser_autoptr

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.AppChooser.get_app_info.html">
	 *     gtk_app_chooser_get_app_info</a>
	 */
	val appInfo: AppInfo?
		get() = gtk_app_chooser_get_app_info(appChooserPointer).wrap()

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.AppChooser.get_content_type.html">
	 *     gtk_app_chooser_get_content_type</a>
	 */
	val contentType: Byte
		get() = gtk_app_chooser_get_content_type(appChooserPointer)!!
			.use { it.pointed.value }

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.AppChooser.refresh.html">
	 *     gtk_app_chooser_refresh</a>
	 */
	fun refresh() {
		gtk_app_chooser_refresh(appChooserPointer)
	}

}