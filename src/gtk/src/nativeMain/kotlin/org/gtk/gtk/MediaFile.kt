package org.gtk.gtk

import gtk.*
import kotlinx.cinterop.reinterpret
import org.gtk.gio.File
import org.gtk.gio.File.Companion.wrap
import org.gtk.gio.InputStream
import org.gtk.gio.InputStream.Companion.wrap

/**
 * 20 / 12 / 2021
 *
 * @see <a href="https://docs.gtk.org/gtk4/class.MediaFile.html">
 *     GtkMediaFile</a>
 */
class MediaFile(
	val mediaFilePointer: GtkMediaFile_autoptr,
) : MediaStream(mediaFilePointer.reinterpret()) {

	constructor() : this(gtk_media_file_new()!!.reinterpret())

	constructor(file: File) :
			this(gtk_media_file_new_for_file(file.filePointer)!!.reinterpret())

	constructor(fileName: String, isResourcePath: Boolean = false) :
			this(if (isResourcePath) {
				gtk_media_file_new_for_resource(fileName)
			} else {
				gtk_media_file_new_for_filename(fileName)
			}!!.reinterpret())

	constructor(stream: InputStream) :
			this(
				gtk_media_file_new_for_input_stream(
					stream.inputStreamPointer
				)!!.reinterpret()
			)

	fun clear() {
		gtk_media_file_clear(mediaFilePointer)
	}

	var file: File?
		get() = gtk_media_file_get_file(mediaFilePointer).wrap()
		set(value) = gtk_media_file_set_file(mediaFilePointer, value?.filePointer)

	var inputStream: InputStream?
		get() = gtk_media_file_get_input_stream(mediaFilePointer).wrap()
		set(value) = gtk_media_file_set_input_stream(mediaFilePointer, value?.inputStreamPointer)

	fun setFilename(filename: String?) {
		gtk_media_file_set_filename(mediaFilePointer, filename)
	}

	fun setResource(resourcePath: String?) {
		gtk_media_file_set_resource(mediaFilePointer, resourcePath)
	}
}