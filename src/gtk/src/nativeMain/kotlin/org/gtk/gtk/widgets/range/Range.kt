package org.gtk.gtk.widgets.range

import glib.gdouble
import glib.gpointer
import gobject.GCallback
import gtk.*
import kotlinx.cinterop.*
import org.gtk.gdk.Rectangle
import org.gtk.glib.bool
import org.gtk.glib.gtk
import org.gtk.gobject.Signals
import org.gtk.gobject.Signals.VALUE_CHANGED
import org.gtk.gobject.addSignalCallback
import org.gtk.gobject.typeCheckInstanceCastOrThrow
import org.gtk.gtk.Adjustment
import org.gtk.gobject.TypedNoArgFunc
import org.gtk.gtk.common.enums.ScrollType
import org.gtk.gtk.widgets.Widget

/**
 * kotlinx-gtk
 * 14 / 03 / 2021
 *
 * @see <a href="https://docs.gtk.org/gtk4/class.Range.html">
 *     GtkRange</a>
 */
open class Range(
	val rangePointer: CPointer<GtkRange>,
) : Widget(rangePointer.reinterpret()) {

	constructor(widget: Widget) :
			this(typeCheckInstanceCastOrThrow(widget, GTK_TYPE_RANGE))

	var fillLevel: Double
		get() = gtk_range_get_fill_level(rangePointer)
		set(value) = gtk_range_set_fill_level(rangePointer, value)

	var restrictToFillLevel: Boolean
		get() = gtk_range_get_restrict_to_fill_level(rangePointer).bool
		set(value) = gtk_range_set_restrict_to_fill_level(
			rangePointer,
			value.gtk
		)

	var showFillLevel: Boolean
		get() = gtk_range_get_show_fill_level(rangePointer).bool
		set(value) = gtk_range_set_show_fill_level(
			rangePointer,
			value.gtk
		)

	var adjustment: Adjustment
		get() = Adjustment(gtk_range_get_adjustment(rangePointer)!!)
		set(value) = gtk_range_set_adjustment(
			rangePointer,
			value.adjustmentPointer
		)

	var isInverted: Boolean
		get() = gtk_range_get_inverted(rangePointer).bool
		set(value) = gtk_range_set_inverted(
			rangePointer,
			value.gtk
		)

	var value: Double
		get() = gtk_range_get_value(rangePointer)
		set(value) = gtk_range_set_value(rangePointer, value)

	fun setIncrements(step: Double, page: Double) {
		gtk_range_set_increments(rangePointer, step, page)
	}

	fun setRange(min: Double, max: Double) {
		gtk_range_set_range(rangePointer, min, max)
	}

	var roundDigits: Int
		get() = gtk_range_get_round_digits(rangePointer)
		set(value) = gtk_range_set_round_digits(rangePointer, value)

	var isFlippable: Boolean
		get() = gtk_range_get_flippable(rangePointer).bool
		set(value) = gtk_range_set_flippable(
			rangePointer,
			value.gtk
		)

	val rangeRect: Rectangle
		get() {
			val rectanglePointer = cValue<GdkRectangle>()
			gtk_range_get_range_rect(rangePointer, rectanglePointer)
			return memScoped {
				Rectangle(rectanglePointer.ptr)
			}
		}

	val sliderRange: IntRange
		get() {
			val start = cValue<IntVar>()
			val end = cValue<IntVar>()
			gtk_range_get_slider_range(rangePointer, start, end)
			return memScoped {
				IntRange(start.ptr.pointed.value, end.ptr.pointed.value)
			}
		}

	var sliderSizeFixed: Boolean
		get() = gtk_range_get_slider_size_fixed(rangePointer).bool
		set(value) = gtk_range_set_slider_size_fixed(
			rangePointer,
			value.gtk
		)

	fun addOnAdjustBoundsCallback(action: RangeAdjustBoundsFunc) =
		addSignalCallback(Signals.ADJUST_BOUNDS, action, staticAdjustBoundsFunc)

	fun addOnChangeValueCallback(action: RangeChangeValueFunc) =
		addSignalCallback(Signals.CHANGE_VALUE, action, staticChangeValueFunc)

	fun addOnMoveSliderCallback(action: RangeMoveSliderFunc) =
		addSignalCallback(Signals.CHANGE_VALUE, action, staticMoveSliderFunc)

	fun addOnValueChangedCallback(action: TypedNoArgFunc<Range>) =
		addSignalCallback(VALUE_CHANGED, action, staticNoArgGCallbackFunc)


	companion object {
		inline fun GtkRange_autoptr?.wrap() =
			this?.wrap()

		inline fun GtkRange_autoptr.wrap() =
			Range(this)

		private val staticAdjustBoundsFunc: GCallback =
			staticCFunction {
					self: GtkRange_autoptr,
					value: gdouble,
					data: gpointer,
				->
				data.asStableRef<RangeAdjustBoundsFunc>()
					.get()
					.invoke(self.wrap(), value)
			}.reinterpret()

		private val staticChangeValueFunc: GCallback =
			staticCFunction {
					self: GtkRange_autoptr,
					scroll: GtkScrollType,
					value: gdouble,
					data: gpointer,
				->
				data.asStableRef<RangeChangeValueFunc>()
					.get()
					.invoke(self.wrap(), ScrollType.valueOf(scroll), value).gtk
			}.reinterpret()

		private val staticMoveSliderFunc: GCallback =
			staticCFunction {
					self: GtkRange_autoptr,
					step: GtkScrollType,
					data: gpointer,
				->
				data.asStableRef<RangeMoveSliderFunc>()
					.get()
					.invoke(self.wrap(), ScrollType.valueOf(step))
			}.reinterpret()

		private val staticNoArgGCallbackFunc: GCallback =
			staticCFunction { self: GtkRange_autoptr, data: gpointer ->
				data.asStableRef<TypedNoArgFunc<Range>>()
					.get()
					.invoke(self.wrap())
			}.reinterpret()

	}
}

typealias RangeAdjustBoundsFunc =
		Range.(
			value: Double,
		) -> Unit

typealias RangeChangeValueFunc =
		Range.(
			scroll: ScrollType,
			value: Double,
		) -> Boolean

typealias RangeMoveSliderFunc =
		Range.(
			step: ScrollType,
		) -> Unit