package org.gtk.gtk.sorter

import gtk.GtkCustomSorter_autoptr
import gtk.gtk_custom_sorter_new
import gtk.gtk_custom_sorter_set_sort_func
import kotlinx.cinterop.reinterpret
import org.gtk.glib.CompareDataFunction
import org.gtk.glib.asStablePointer
import org.gtk.glib.staticCompareDataFunction
import org.gtk.gobject.staticDestroyStableRefFunction

/**
 * 17 / 12 / 2021
 *
 * @see <a href="https://docs.gtk.org/gtk4/class.CustomSorter.html">
 *     GtkCustomSorter</a>
 */
open class CustomSorter(val customSorterPointer: GtkCustomSorter_autoptr) :
	Sorter(customSorterPointer.reinterpret()) {

	constructor(sortFunction: CompareDataFunction) :
			this(
				gtk_custom_sorter_new(
					staticCompareDataFunction,
					sortFunction.asStablePointer(),
					staticDestroyStableRefFunction
				)!!
			)

	fun setSortFunction(sortFunction: CompareDataFunction?) {
		if (sortFunction == null) {
			gtk_custom_sorter_set_sort_func(
				customSorterPointer,
				null,
				null,
				null
			)
			return
		}
		gtk_custom_sorter_set_sort_func(
			customSorterPointer,
			staticCompareDataFunction,
			sortFunction.asStablePointer(),
			staticDestroyStableRefFunction
		)
	}
}