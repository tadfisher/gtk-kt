package org.gtk.gtk.manager

import gtk.GtkBinLayout_autoptr
import gtk.gtk_bin_layout_new
import kotlinx.cinterop.reinterpret

/**
 * kotlinx-gtk
 *
 * 22 / 11 / 2021
 *
 * @see <a href="https://docs.gtk.org/gtk4/class.BinLayout.html">
 *     GtkBinLayout</a>
 */
class BinLayout(val binLayoutPointer: GtkBinLayout_autoptr) :
	LayoutManager(binLayoutPointer.reinterpret()) {

	constructor() : this(gtk_bin_layout_new()!!.reinterpret())
}