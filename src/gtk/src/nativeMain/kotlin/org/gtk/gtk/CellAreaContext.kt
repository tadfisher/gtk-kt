package org.gtk.gtk

import gtk.*
import kotlinx.cinterop.*
import org.gtk.gobject.KGObject
import org.gtk.gtk.CellArea.Companion.wrap
import org.gtk.gtk.common.data.Dimensions
import org.gtk.gtk.common.data.PreferredHeight
import org.gtk.gtk.common.data.PreferredWidth

/**
 * 26 / 03 / 2021
 *
 * @see <a href="https://docs.gtk.org/gtk4/class.CellAreaContext.html">
 *     GtkCellAreaContext</a>
 */
class CellAreaContext(
	val cellAreaContextPointer: GtkCellAreaContext_autoptr,
) : KGObject(cellAreaContextPointer.reinterpret()) {

	fun allocate(width: Int, height: Int) {
		gtk_cell_area_context_allocate(
			cellAreaContextPointer,
			width,
			height
		)
	}

	val allocation: Dimensions
		get() = memScoped {
			val width = cValue<IntVar>()
			val height = cValue<IntVar>()

			gtk_cell_area_context_get_allocation(
				cellAreaContextPointer,
				width,
				height
			)

			Dimensions(
				width.ptr.pointed.value,
				height.ptr.pointed.value
			)
		}

	val area: CellArea
		get() = gtk_cell_area_context_get_area(cellAreaContextPointer)!!.wrap()


	fun getPreferredHeight(
	): PreferredHeight =
		memScoped {
			val minimumHeight = cValue<IntVar>()
			val naturalHeight = cValue<IntVar>()

			gtk_cell_area_context_get_preferred_height(
				cellAreaContextPointer,
				minimumHeight,
				naturalHeight
			)

			PreferredHeight(
				minimumHeight.ptr.pointed.value,
				naturalHeight.ptr.pointed.value
			)
		}

	fun getPreferredHeightForWidth(
		width: Int,
	): PreferredHeight =
		memScoped {
			val minimumHeight = cValue<IntVar>()
			val naturalHeight = cValue<IntVar>()

			gtk_cell_area_context_get_preferred_height_for_width(
				cellAreaContextPointer,
				width,
				minimumHeight,
				naturalHeight,
			)

			PreferredHeight(
				minimumHeight.ptr.pointed.value,
				naturalHeight.ptr.pointed.value
			)
		}


	fun getPreferredWidth(
	): PreferredWidth =
		memScoped {
			val minimumWidth = cValue<IntVar>()
			val naturalWidth = cValue<IntVar>()

			gtk_cell_area_context_get_preferred_width(
				cellAreaContextPointer,
				minimumWidth,
				naturalWidth
			)

			PreferredWidth(
				minimumWidth.ptr.pointed.value,
				naturalWidth.ptr.pointed.value
			)
		}

	fun getPreferredWidthForHeight(
		height: Int,
	): PreferredWidth =
		memScoped {
			val minimumWidth = cValue<IntVar>()
			val naturalWidth = cValue<IntVar>()

			gtk_cell_area_context_get_preferred_width_for_height(
				cellAreaContextPointer,
				height,
				minimumWidth,
				naturalWidth,
			)

			PreferredWidth(
				minimumWidth.ptr.pointed.value,
				naturalWidth.ptr.pointed.value
			)
		}

	fun pushPreferredHeight(minimumHeight: Int, naturalHeight: Int) {
		gtk_cell_area_context_push_preferred_height(
			cellAreaContextPointer,
			minimumHeight,
			naturalHeight
		)
	}

	fun pushPreferredWidth(minimumWidth: Int, naturalWidth: Int) {
		gtk_cell_area_context_push_preferred_width(
			cellAreaContextPointer,
			minimumWidth,
			naturalWidth
		)
	}

	fun reset() {
		gtk_cell_area_context_reset(cellAreaContextPointer)
	}

	companion object {
		inline fun GtkCellAreaContext_autoptr?.wrap() =
			this?.wrap()

		inline fun GtkCellAreaContext_autoptr.wrap() =
			CellAreaContext(this)
	}
}