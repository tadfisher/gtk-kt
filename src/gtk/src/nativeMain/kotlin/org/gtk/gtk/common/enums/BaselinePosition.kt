package org.gtk.gtk.common.enums

import gtk.GtkBaselinePosition
import gtk.GtkBaselinePosition.*

/**
 * kotlinx-gtk
 * 06 / 03 / 2021
 */
enum class BaselinePosition(val gtk: GtkBaselinePosition) {
	TOP(GTK_BASELINE_POSITION_TOP),
	CENTER(GTK_BASELINE_POSITION_CENTER),
	BOTTOM(GTK_BASELINE_POSITION_BOTTOM);

	companion object {

		fun valueOf(gtk: GtkBaselinePosition): BaselinePosition =
			when (gtk) {
				GTK_BASELINE_POSITION_TOP -> TOP
				GTK_BASELINE_POSITION_CENTER -> CENTER
				GTK_BASELINE_POSITION_BOTTOM -> BOTTOM
			}
	}
}