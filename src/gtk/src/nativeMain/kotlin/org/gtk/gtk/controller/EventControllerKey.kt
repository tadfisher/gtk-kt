package org.gtk.gtk.controller

import glib.gpointer
import gobject.GCallback
import gtk.*
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.asStableRef
import kotlinx.cinterop.reinterpret
import kotlinx.cinterop.staticCFunction
import org.gtk.glib.gtk
import org.gtk.gobject.Signals
import org.gtk.gobject.TypeInstance
import org.gtk.gobject.addSignalCallback
import org.gtk.gobject.typeCheckInstanceCastOrThrow
import org.gtk.gtk.IMContext
import org.gtk.gtk.IMContext.Companion.wrap
import org.gtk.gobject.TypedNoArgFunc
import org.gtk.gtk.widgets.Widget

/**
 * 06 / 01 / 2022
 *
 * @see <a href="https://docs.gtk.org/gtk4/class.EventControllerKey.html">
 *     GtkEventControllerKey</a>
 */
class EventControllerKey(
	val keyPointer: CPointer<GtkEventControllerKey>,
) : EventController(keyPointer.reinterpret()) {

	constructor() :
			this(gtk_event_controller_key_new()!!.reinterpret())

	constructor(obj: TypeInstance) : this(typeCheckInstanceCastOrThrow(
		obj,
		GTK_TYPE_EVENT_CONTROLLER_KEY
	))

	fun forward(widget: Widget) {
		gtk_event_controller_key_forward(
			keyPointer,
			widget.widgetPointer
		)
	}

	val group: UInt
		get() = gtk_event_controller_key_get_group(keyPointer)

	var imContext: IMContext
		get() = gtk_event_controller_key_get_im_context(keyPointer)!!.wrap()
		set(value) = gtk_event_controller_key_set_im_context(
			keyPointer,
			value.imContextPointer
		)

	fun addOnIMUpdateCallback(action: TypedNoArgFunc<EventControllerKey>) =
		addSignalCallback(
			Signals.IM_UPDATE,
			action,
			staticNoArgGCallback
		)

	fun addOnKeyPressedCallback(action: EventControllerKeyPressedFunc) =
		addSignalCallback(
			Signals.KEY_PRESSED,
			action,
			staticPressedFunc
		)

	fun addOnKeyReleasedCallback(action: EventControllerKeyReleasedFunc) =
		addSignalCallback(
			Signals.KEY_RELEASED,
			action,
			staticReleasedFunc
		)

	fun addOnModifiersCallback(action: EventControllerKeyModifiersFunc) =
		addSignalCallback(
			Signals.MODIFIERS,
			action,
			staticModifiersFunc
		)

	companion object {

		inline fun CPointer<GtkEventControllerKey>?.wrap() =
			this?.wrap()

		inline fun CPointer<GtkEventControllerKey>.wrap() =
			EventControllerKey(this)

		private val staticNoArgGCallback: GCallback =
			staticCFunction {
					self: CPointer<GtkEventControllerKey>,
					data: gpointer,
				->
				data.asStableRef<TypedNoArgFunc<EventControllerKey>>()
					.get()
					.invoke(self.wrap())
			}.reinterpret()

		private val staticPressedFunc: GCallback =
			staticCFunction {
					self: CPointer<GtkEventControllerKey>,
					keyval: UInt,
					keycode: UInt,
					state: GdkModifierType,
					data: gpointer,
				->
				data.asStableRef<EventControllerKeyPressedFunc>()
					.get()
					.invoke(self.wrap(), keyval, keycode, state)
					.gtk
			}.reinterpret()

		private val staticReleasedFunc: GCallback =
			staticCFunction {
					self: CPointer<GtkEventControllerKey>,
					keyval: UInt,
					keycode: UInt,
					state: GdkModifierType,
					data: gpointer,
				->
				data.asStableRef<EventControllerKeyReleasedFunc>()
					.get()
					.invoke(self.wrap(), keyval, keycode, state)
			}.reinterpret()

		private val staticModifiersFunc: GCallback =
			staticCFunction {
					self: CPointer<GtkEventControllerKey>,
					keyval: GdkModifierType,
					data: gpointer,
				->
				data.asStableRef<EventControllerKeyModifiersFunc>()
					.get()
					.invoke(self.wrap(), keyval)
			}.reinterpret()

	}
}

typealias EventControllerKeyPressedFunc =
		EventControllerKey.(
			keyVal: UInt,
			keycode: UInt,
			state: GdkModifierType,
		) -> Boolean

typealias EventControllerKeyReleasedFunc =
		EventControllerKey.(
			keyVal: UInt,
			keycode: UInt,
			state: GdkModifierType,
		) -> Unit

typealias EventControllerKeyModifiersFunc =
		EventControllerKey.(
			keVal: GdkModifierType,
		) -> Boolean