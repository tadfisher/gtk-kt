package org.gtk.gtk.widgets

import glib.gpointer
import gobject.GCallback
import gtk.*
import kotlinx.cinterop.*
import org.gtk.gio.Icon
import org.gtk.glib.CStringPointer
import org.gtk.glib.bool
import org.gtk.glib.gtk
import org.gtk.gobject.Signals
import org.gtk.gobject.addSignalCallback
import org.gtk.gobject.typeCheckInstanceCastOrThrow
import org.gtk.gtk.AppChooser
import org.gtk.gobject.TypedNoArgFunc

/**
 * kotlinx-gtk
 *
 * 07 / 07 / 2021
 *
 * @see <a href="https://docs.gtk.org/gtk4/class.AppChooserButton.html">
 *     GtkAppChooserButton</a>
 */
class AppChooserButton(val appChooserButton: GtkAppChooserButton_autoptr) :
	Widget(appChooserButton.reinterpret()), AppChooser {

	override val appChooserPointer: GtkAppChooser_autoptr by lazy {
		appChooserButton.reinterpret()
	}

	constructor(widget: Widget) :
			this(typeCheckInstanceCastOrThrow(widget, GTK_TYPE_APP_CHOOSER_BUTTON))

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/ctor.AppChooserButton.new.html">
	 *     gtk_app_chooser_button_new</a>
	 */
	constructor(contentType: String) : this(gtk_app_chooser_button_new(contentType)!!.reinterpret())

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.AppChooserButton.append_custom_item.html">
	 *     gtk_app_chooser_button_append_custom_item</a>
	 */
	fun appendCustomItem(name: String, label: String, icon: Icon) {
		gtk_app_chooser_button_append_custom_item(appChooserButton, name, label, icon.iconPointer)
	}

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.AppChooserButton.append_separator.html">
	 *     gtk_app_chooser_button_append_separator</a>
	 */
	fun appendSeparator() {
		gtk_app_chooser_button_append_separator(appChooserButton)
	}

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.AppChooserButton.get_heading.html">
	 *     gtk_app_chooser_button_get_heading</a>
	 * @see <a href="https://docs.gtk.org/gtk4/method.AppChooserButton.set_heading.html">
	 *     gtk_app_chooser_button_set_heading</a>
	 */
	var heading: String?
		get() = gtk_app_chooser_button_get_heading(appChooserButton)?.toKString()
		set(value) = gtk_app_chooser_button_set_heading(appChooserButton, value)

	var modal: Boolean
		get() = gtk_app_chooser_button_get_modal(appChooserButton).bool
		set(value) = gtk_app_chooser_button_set_modal(appChooserButton, value.gtk)

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.AppChooserButton.get_show_default_item.html">
	 *     gtk_app_chooser_button_get_show_default_item</a>
	 * @see <a href="https://docs.gtk.org/gtk4/method.AppChooserButton.set_show_default_item.html">
	 *     gtk_app_chooser_button_set_show_default_item</a>
	 */
	var showDefaultItem: Boolean
		get() = gtk_app_chooser_button_get_show_default_item(appChooserButton).bool
		set(value) = gtk_app_chooser_button_set_show_default_item(appChooserButton, value.gtk)

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.AppChooserButton.get_show_dialog_item.html">
	 *     gtk_app_chooser_button_get_show_dialog_item</a>
	 * @see <a href="https://docs.gtk.org/gtk4/method.AppChooserButton.set_show_dialog_item.html">
	 *     gtk_app_chooser_button_set_show_dialog_item</a>
	 */
	var showDialogItem: Boolean
		get() = gtk_app_chooser_button_get_show_dialog_item(appChooserButton).bool
		set(value) = gtk_app_chooser_button_set_show_dialog_item(appChooserButton, value.gtk)

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.AppChooserButton.set_active_custom_item.html">
	 *     gtk_app_chooser_button_set_active_custom_item</a>
	 */
	fun setActiveCustomItem(name: String) {
		gtk_app_chooser_button_set_active_custom_item(appChooserButton, name)
	}

	fun addOnActivateCallback(action: TypedNoArgFunc<AppChooserButton>) =
		addSignalCallback(Signals.ACTIVATE, action, staticNoArgFunc)

	fun addOnChangedCallback(action: TypedNoArgFunc<AppChooserButton>) =
		addSignalCallback(Signals.CHANGED, action, staticNoArgFunc)

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/signal.AppChooserButton.custom-item-activated.html">
	 *     custom-item-activated</a>
	 */
	fun addOnCustomItemActivatedCallback(action: AppChooserButtonCustomFunc) {
		addSignalCallback(Signals.CUSTOM_ITEM_ACTIVATED, action, staticCStringCallback)
	}

	companion object {
		private val staticNoArgFunc: GCallback =
			staticCFunction {
					self: GtkAppChooserButton_autoptr,
					data: gpointer,
				->
				data.asStableRef<TypedNoArgFunc<AppChooserButton>>()
					.get()
					.invoke(self.wrap())
				Unit
			}.reinterpret()

		private val staticCStringCallback: GCallback =
			staticCFunction {
					self: GtkAppChooserButton_autoptr,
					arg1: CStringPointer,
					data: gpointer,
				->
				data.asStableRef<AppChooserButtonCustomFunc>()
					.get()
					.invoke(self.wrap(), arg1.toKString())
				Unit
			}.reinterpret()

		inline fun GtkAppChooserButton_autoptr?.wrap() =
			this?.wrap()

		inline fun GtkAppChooserButton_autoptr.wrap() =
			AppChooserButton(this)
	}
}

typealias AppChooserButtonCustomFunc =
		AppChooserButton.(
			itemName: String,
		) -> Unit