package org.gtk.gdk

import gtk.GdkAxisFlags
import gtk.GdkTimeCoord

/**
 * 03 / 01 / 2022
 *
 * @see <a href="https://docs.gtk.org/gdk4/struct.TimeCoord.html">
 *     GdkTimeCoord</a>
 */
class TimeCoord(
	val struct: GdkTimeCoord,
) {
	var time: UInt
		get() = struct.time
		set(value) {
			struct.time = value
		}

	var flags: GdkAxisFlags
		get() = struct.flags
		set(value) {
			struct.flags = value
		}

	// TODO axes
}