package org.gtk.gsk

import gtk.GskParseLocation
import kotlinx.cinterop.CPointer
import org.gtk.gtk.widgets.Widget

/**
 * kotlinx-gtk
 *
 * 13 / 10 / 2021
 *
 * @see <a href=""></a>
 */
class ParseLocation(val parseLocationPointer: CPointer<GskParseLocation>) {

	companion object {
		inline fun CPointer<GskParseLocation>?.wrap() =
			this?.wrap()

		inline fun CPointer<GskParseLocation>.wrap() =
			ParseLocation(this)
	}
}