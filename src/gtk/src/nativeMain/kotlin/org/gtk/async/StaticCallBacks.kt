package org.gtk.async

import glib.gboolean
import glib.gpointer
import gobject.GCallback
import kotlinx.cinterop.*
import org.gtk.glib.*
import org.gtk.gobject.SignalManager
import org.gtk.gobject.Signals
import org.gtk.gobject.signalManager
import org.gtk.gtk.WidgetPointer
import org.gtk.gtk.widgets.Widget
import org.gtk.gtk.widgets.Widget.Companion.wrap

// This file contains generic static callbacks that are frequently used in the program
@Deprecated("Self reference instead")
val staticIntCallback: GCallback =
	staticCFunction { _: gpointer?, arg1: Int, data: gpointer? ->
		data?.asStableRef<(Int) -> Unit>()
			?.get()
			?.invoke(arg1)
		Unit
	}.reinterpret()

@Deprecated("Self reference instead")
val staticDoubleCallback: GCallback =
	staticCFunction { _: gpointer?, arg1: Double, data: gpointer? ->
		data?.asStableRef<(Double) -> Unit>()
			?.get()
			?.invoke(arg1)
		Unit
	}.reinterpret()

@Deprecated("Self reference instead")
val staticBooleanCallback: GCallback =
	staticCFunction {
			_: gpointer?,
			arg1: gboolean,
			data: gpointer?,
		->
		data?.asStableRef<(Boolean) -> Unit>()?.get()
			?.invoke(arg1.bool)
		Unit
	}.reinterpret()

@Deprecated("Self reference instead")
val staticCStringCallback: GCallback =
	staticCFunction {
			_: gpointer?,
			arg1: CStringPointer,
			data: gpointer?,
		->
		data?.asStableRef<(String) -> Unit>()?.get()
			?.invoke(arg1.toKString())
		Unit
	}.reinterpret()


/**
 * Used for [org.gtk.gobject.Signals.ACTIVATE_LINK]
 */
@Deprecated("Self reference instead")
internal val staticActivateLinkFunction: GCallback by lazy {
	staticCFunction { _: gpointer?, char: CStringPointer, data: gpointer? ->
		data?.asStableRef<ActivateLinkFunction>()?.get()?.invoke(char.toKString()).gtk
	}.reinterpret()
}

@Deprecated("Self reference instead")
typealias ActivateLinkFunction = (@ParameterName("uri") String) -> Boolean

@Deprecated("Self reference instead")
internal inline fun activateLinkSignalManager(
	pointer: VoidPointer,
	noinline action: ActivateLinkFunction,
) =
	signalManager(
		pointer,
		Signals.ACTIVATE_LINK,
		action.asStablePointer(),
		staticActivateLinkFunction
	)

/**
 * Used for [org.gtk.gobject.Signals.POPULATE_POPUP]
 */
@Deprecated("Self reference instead")
internal val staticPopulatePopupFunction: GCallback =
	staticCFunction { _: gpointer?, previous: WidgetPointer, data: gpointer? ->
		data?.asStableRef<PopulatePopupFunction>()?.get()?.invoke(previous.wrap())
		Unit
	}.reinterpret()

@Deprecated("Self reference instead")
typealias PopulatePopupFunction = (Widget) -> Unit

@Deprecated("Self reference instead")
internal inline fun populatePopupSignalManager(
	pointer: VoidPointer,
	noinline action: PopulatePopupFunction,
) =
	signalManager(
		pointer,
		Signals.POPULATE_POPUP,
		StableRef.create(action).asCPointer(),
		staticPopulatePopupFunction
	)


@Deprecated("Self reference instead")
inline fun popdownSignalManager(pointer: VoidPointer, noinline action: () -> Unit): SignalManager =
	signalManager(pointer, Signals.POPDOWN, action.asStablePointer())


@Deprecated("Self reference instead")
inline fun popupSignalManager(pointer: VoidPointer, noinline action: () -> Unit): SignalManager =
	signalManager(pointer, Signals.POPUP, action.asStablePointer())

