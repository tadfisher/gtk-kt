import org.gtk.gio.File
import org.gtk.glib.use
import kotlin.test.Test

class FileTest {

    @Test
    fun multiLine() {
        File.newForPath("./src/nativeTest/resources/multi-line.txt").use {
            println("Filename: ${it.baseName}")
            println("Contents: ${it.loadContents()}")
        }
    }
}