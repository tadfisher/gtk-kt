package org.gtk.glib

import glib.GPtrArray_autoptr
import glib.g_ptr_array_add
import glib.g_ptr_array_new
import glib.gpointer

/**
 * 18 / 12 / 2021
 *
 * @see <a href="https://docs.gtk.org/glib/struct.PtrArray.html">
 *     GPtrArray</a>
 */
open class PtrArray(
	val ptrArrayPointer: GPtrArray_autoptr,
) {
	constructor() : this(g_ptr_array_new()!!)

	fun add(data: gpointer) {
		g_ptr_array_add(ptrArrayPointer, data)
	}
}