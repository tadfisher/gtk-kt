package org.gtk.glib

import glib.*
import kotlinx.cinterop.CPointer
import org.gtk.glib.Variant.Companion.wrap

/**
 * gtk-kt
 *
 * 16 / 10 / 2021
 *
 * @see <a href="https://docs.gtk.org/glib/struct.VariantDict.html">
 *     GVariantDict</a>
 */
open class VariantDictionary(val variantDictPointer: CPointer<GVariantDict>) :
	UnrefMe {

	/**
	 * @see <a href="https://docs.gtk.org/glib/ctor.VariantDict.new.html">
	 *     g_variant_dict_new</a>
	 */
	constructor(fromAsv: Variant? = null) :
			this(g_variant_dict_new(fromAsv?.variantPointer)!!)

	/**
	 * @see <a href="https://docs.gtk.org/glib/method.VariantDict.clear.html">
	 *     g_variant_dict_clear</a>
	 */
	fun clear() {
		g_variant_dict_clear(variantDictPointer)
	}

	/**
	 * This is an operator function as it directly correlates in naming.
	 *
	 * @see <a href="https://docs.gtk.org/glib/method.VariantDict.contains.html">
	 *     g_variant_dict_contains</a>
	 */
	operator fun contains(key: String): Boolean =
		g_variant_dict_contains(variantDictPointer, key).bool

	/**
	 * @see <a href="https://docs.gtk.org/glib/method.VariantDict.end.html">
	 *     g_variant_dict_end</a>
	 */
	fun end(): Variant =
		g_variant_dict_end(variantDictPointer)!!.wrap()

	/**
	 * @see <a href="https://docs.gtk.org/glib/method.VariantDict.insert_value.html">
	 *     g_variant_dict_insert_value</a>
	 */
	fun insert(key: String, variant: Variant) {
		g_variant_dict_insert_value(variantDictPointer, key, variant.variantPointer)
	}

	/**
	 * @see <a href="https://docs.gtk.org/glib/method.VariantDict.lookup_value.html">
	 *     g_variant_dict_lookup_value</a>
	 */
	fun lookup(key: String, type: VariantType?): Variant? =
		g_variant_dict_lookup_value(variantDictPointer, key, type?.variantTypePointer).wrap()

	/**
	 * @see <a href="https://docs.gtk.org/glib/method.VariantDict.ref.html">
	 *     g_variant_dict_ref</a>
	 */
	fun ref() = g_variant_dict_ref(variantDictPointer)!!.wrap()

	/**
	 * @see <a href="https://docs.gtk.org/glib/method.VariantDict.remove.html">
	 *     g_variant_dict_remove</a>
	 */
	fun remove(key: String): Boolean =
		g_variant_dict_remove(variantDictPointer, key).bool

	/**
	 * @see <a href="https://docs.gtk.org/glib/method.VariantDict.unref.html">
	 *     g_variant_dict_unref</a>
	 */
	override fun unref() {
		g_variant_dict_unref(variantDictPointer)
	}

	companion object {
		inline fun CPointer<GVariantDict>?.wrap() =
			this?.wrap()

		inline fun CPointer<GVariantDict>.wrap() =
			VariantDictionary(this)
	}
}