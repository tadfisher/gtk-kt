package org.gtk.dsl.gtk

import org.gtk.dsl.GtkDsl
import org.gtk.gtk.widgets.box.Box
import org.gtk.gtk.widgets.button.toggleable.checkable.CheckButton
import org.gtk.gtk.widgets.frame.Frame

@GtkDsl
fun Box.checkButton(
	label: String? = null,
	mnemonic: Boolean = false,
	append: Boolean = true,
	buttonBuilder: CheckButton.() -> Unit = {}
): CheckButton = (label?.let { CheckButton(it, mnemonic) } ?: CheckButton())
	.apply(buttonBuilder)
	.also { if (append) append(it) else prepend(it) }

@GtkDsl
fun Frame.checkButton(
	label: String? = null,
	mnemonic: Boolean = false,
	buttonBuilder: CheckButton.() -> Unit = {}
): CheckButton = (label?.let { CheckButton(it, mnemonic) } ?: CheckButton())
	.apply(buttonBuilder)
	.also { child = it }
