package org.gtk.dsl.gio

import org.gtk.dsl.GtkDsl
import org.gtk.gio.ActionMap
import org.gtk.gio.SimpleAction

@GtkDsl
fun SimpleAction.enable() {
	setEnabled(true)
}

@GtkDsl
fun SimpleAction.disable() {
	setEnabled(false)
}

@GtkDsl
fun simpleAction(
	name: String,
	action: SimpleAction.() -> Unit
): SimpleAction = SimpleAction(name).apply(action)

@GtkDsl
fun ActionMap.addSimpleAction(name: String, action: SimpleAction.() -> Unit) {
	addAction(simpleAction(name, action))
}